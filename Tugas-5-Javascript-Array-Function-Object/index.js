//soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var sortdaftarHewan = daftarHewan.sort()
    for(var x = 0; x < sortdaftarHewan.length; x++){
        console.log(sortdaftarHewan[x])
    }

//soal 2
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
function introduce(data){
    return "Nama saya "+data.name+", umur saya "+data.age+", alamat saya di "+data.address+", dan saya hobby yaitu "+data.hobby 
}
var perkenalan = introduce(data)
console.log(perkenalan)

//soal 3
function hitung_huruf_vokal(string){
    var toString = string.toLowerCase().split('')
    var vocal = ["a", "i", "u", "e", "o"];
    var jml_vocal= 0;

    for (var x = 0; x < toString.length; x++){
        for (var y = 0; y < vocal.length; y++){
            if (toString[x] == vocal[y]){
                jml_vocal += 1;
            } else {
                jml_vocal += 0;
            }
        }
    }
    return jml_vocal;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log(hitung_1 , hitung_2)

//soal 4

function hitung(int) {
    return (int - 1) * 2;
}

console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(5));
