// soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
var upper = kedua.substr(8, 9).toUpperCase();

console.log(pertama.substr(0, 5).concat(pertama.substring(12, 19)).concat(kedua.substr(0, 8)).concat(upper));

// soal 2
var kataPertama = Number("10");
var kataKedua = Number("2");
var kataKetiga = Number("4");
var kataKeempat = Number("6");

console.log(kataPertama + kataKedua * kataKetiga + kataKeempat);

// soal 3
var kalimat = 'wah javascript itu keren sekali'; 
var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14);  
var kataKetiga = kalimat.substring(15, 18);  
var kataKeempat = kalimat.substring(19, 24);  
var kataKelima = kalimat.substring(25, 31);  

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);