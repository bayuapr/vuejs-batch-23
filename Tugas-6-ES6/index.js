// soal 1
const functionLuas = (panjang1, panjang2) => {
  return (panjang1 * panjang2)
}

const functionKeliling = (panjang1, panjang2) => {
  return (2*panjang1) + (2*panjang2)
}
console.log("Luas : ",functionLuas(2,4))
console.log("Keliling : ",functionKeliling(2,4))

//soal 2
const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => { 
      console.log(`${firstName} ${lastName}`)
    }
  } 
}
// Driver Code 
newFunction("William", "Imoh").fullName() 

// soal 3
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}
const{firstName, lastName, address, hobby} = newObject
// Driver code
console.log(firstName, lastName, address, hobby)

// soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)

//Driver Code
console.log(combined)

// soal 5
const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet  
// Driver Code
console.log(before) 
var after = `Lorem ${view}dolar sit amet, consectetur adipiscing elit,${planet}`
console.log(after) 